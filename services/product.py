
from uuid import UUID
from database.product import DBProduct
from models.product import Product


class ProductService:
    """
    This is the product service for performing all operations that involve the product,
    including crud, auth etc
    """

    def __init__(self, database):
        self.db = database
    