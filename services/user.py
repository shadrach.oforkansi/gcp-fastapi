from typing import List
from uuid import UUID
from database.user import DBUser
from models.user import User
from services.utils.password_codec import PasswordHash


class UserService:
    """
    This is the user service for performing all operations that involve the user,
    including crud, auth etc
    """

    def __init__(self, database):
        self.db = database
        
    
    async def list_users(self) -> List[User]:
        """ This function returns a list of all users in the database
        """
        users = await DBUser(self.db).list()
        return users
    
    async def get_user(self, id:UUID) -> User:
        """ Gets a user by id
        
        Args: 
            id: The id of the user to get
        
        Returns:
            user[User]: The user with the given id
            
        Raises:
            Exception: If the user with the given id does not exist
            
        """
        user = await DBUser(self.db).get(id)
        if user:
            print(user.dict())
            return user
        raise Exception("User not found")
    
    async def add_user(self, user:User) -> User:
        """ Creates a new user account
        
        Args:
            user: The user to create
            
        Returns:
            user[User]: The user that was created
            
        """
        user.password = PasswordHash.hash_password(user.password)
        user_stored = await DBUser(self.db).add(user)
        return user_stored
    
    async def update_user(self, id:UUID, user:User):
        """ Updates user account except password
        
        Args:
            id[UUID]: id of user to update
            user: new user detail to update
            
        Returns:
            user[User]: The user that was updated
            
        Raises:
            Exception: If the user with the given id does not exist
            
        """
        stored_user = await DBUser(self.db).get(id)
        if stored_user:
            stored_user = stored_user.copy(update=user.dict())
            updated_user = await DBUser(self.db).update(id, stored_user)
            return updated_user
        raise Exception("User not found")

    async def delete_user(self, id:UUID):
        """ Deletes a user account
        
        Args:
            id[UUID]: id of user to delete
            
        Returns:
            None
        
        Raises:
            Exception: If the user with the given id does not exist
        """
        if await DBUser(self.db).get(id):
            await DBUser(self.db).delete(id)
            return None
        raise Exception("User not found")
