
from uuid import UUID

from pymongo.database import Database

from models.user import User
class DBUser:
    """
    This is the user mongo database repository
    """

    def __init__(self, database: Database):
        self.user = database["user"]

    async def get(self, id: UUID):
        query = {"id": id}
        user = self.user.find_one(query)
        if user is None:
            return None
        return User(**user)
        
    async def list(self):
        users = self.user.find()
        users = [User(**user) for user in users]
        return users

    async def add(self, user: User):
        self.user.insert_one(user.dict())
        return user

    async def update(self, id: UUID, user: User):
        query = {"id": id}
        newvalues = {"$set": user.dict()}
        self.user.update_one(query, newvalues)
        user = self.user.find_one(query)
        return User(**user)

    async def delete(self, id: UUID):
        query = {"id": id}
        self.user.delete_one(query)
        return
        