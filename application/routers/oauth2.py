
import datetime
from uuid import UUID

from fastapi import Depends
from jose import JWTError, jwt

from config import ALGORITHM, EXPIRATION_TIME, SECRET_KEY



def create_access_token(data: dict) -> str:
    """
    This will create a jwt access token based on the data passed
    """
    to_encode = data.copy()
    expire = datetime.datetime.utcnow() + datetime.timedelta(minutes=EXPIRATION_TIME)
    to_encode.update({"exp": expire})
    token = jwt.encode(to_encode, key=SECRET_KEY, algorithm=ALGORITHM)
    return token


def verify_access_token(token: str, credentials_exception):
    """
    This will verify an access token and return the uuid of the user
    in the token
    """
    try:
        payload = jwt.decode(token, key=SECRET_KEY, algorithms=[ALGORITHM])
        user_id: str = payload.get("id")
        if not user_id:
            raise credentials_exception
        return UUID(user_id)
    except JWTError:
        raise credentials_exception
    