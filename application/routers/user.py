
from typing import List
from uuid import UUID

from fastapi import APIRouter, BackgroundTasks, Depends, status
from fastapi.exceptions import HTTPException

from application.schemas import UserIn, UserOut
from application.dependencies import get_database
from models.user import User
from services.user import UserService

router = APIRouter(prefix="/users", tags=["USER"])

@router.get("/", response_model=List[UserOut])
async def list_users(db: Depends = Depends(get_database)):
    """ list of all users in the database
    """
    try:
        users = await UserService(db).list_users()
        return [UserOut(**user.dict()) for user in users]
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, 
            detail=str(e).lower())

@router.post("/", response_model=UserOut)
async def create_user(user: UserIn, background_task: BackgroundTasks, db: Depends = Depends(get_database)):
    """ Creates user in the database
    """
    try:
        user = await UserService(db).add_user(user)
        user_service = UserService(db)
        background_task.add_task(user_service.get_user, user.id)
        return UserOut(**user.dict())
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, 
            detail=str(e).lower())
        

@router.get("/{id}", response_model=UserOut)
async def get_user(id:UUID, db: Depends = Depends(get_database)):
    """ This function gets a user by id
    """
    try:
        user = await UserService(db).get_user(id)
        return UserOut(**user.dict())
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, 
            detail=str(e).lower())
        

