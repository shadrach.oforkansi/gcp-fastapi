
import pymongo

from config import DATABASE_URL


def get_database():
    """
    This function returns a database conneciton for a mongodb database
    """
    client = pymongo.MongoClient(DATABASE_URL)
    db = client["gcp_fastapi"]
    return db
    