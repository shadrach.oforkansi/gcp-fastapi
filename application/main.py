
from fastapi import FastAPI
from fastapi.openapi.utils import get_openapi
from application.routers import user

app = FastAPI(
    title="Gcp Fastapi",
    description="This is the API backend service for the Gcp Fastapisystem",
)

@app.get("/status")
def status():
    return {"status": "{project_name} api service running successfully"}


@app.get("/")
def root():
    return {
        "message": "welcome to {project_name} api service, go to '/docs' to view the documentation for the project"
    }

@app.get("/metrics")
def metrics():
    return {
        "metrics": {
            "state": "running",
        }
    }



# include sub routers
app.include_router(user.router),

# customize open api documentation
def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="Gcp Fastapi REST API Service",
        version="1.0.0",
        description="This is the API documentation for the Gcp Fastapi API Service",
        routes=app.routes,
    )
    openapi_schema["info"]["x-logo"] = {
        "url": "https://i0.wp.com/www.intelregion.com/wp-content/uploads/2020/10/eHealth4everyone-1.jpg"
    }
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi

if __name__ == "__main__":
    uvicorn.run(app, port=8000, reload=True)
    