
from fastapi import Depends, HTTPException

from application.dependencies import get_database
from fastapi import status

from application.routers.oauth2 import verify_access_token
from fastapi.security import OAuth2PasswordBearer

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")
    