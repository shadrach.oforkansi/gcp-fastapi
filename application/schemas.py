
import datetime
from uuid import UUID
from pydantic import BaseModel, Field
from pydantic.networks import EmailStr  

class UserOut(BaseModel):
    id: UUID
    first_name: str
    last_name: str
    email: EmailStr
    
class UserIn(BaseModel):
    first_name: str
    last_name: str
    email: EmailStr
    password: str
    
