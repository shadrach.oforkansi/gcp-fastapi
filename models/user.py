from enum import Enum
from uuid import UUID, uuid4
from pydantic import BaseModel, EmailStr, Field
import datetime


class User(BaseModel):
    id: UUID = Field(default_factory=uuid4)
    first_name: str
    last_name: str
    email: EmailStr
    password: str
    